<?php

namespace App\Services;

use Stringy\Stringy;

class RoutingService
{
    private $routes = [];

    public function getRelativeUri()
    {
        return Stringy::create($_SERVER['REQUEST_URI'])// /autoloading/users
            ->removeLeft(config('app_base_path8'))
            ->__toString();

    }

    public function map($method, $uriPattern, $controllerAction)
    {
        $this->routes[$method][$uriPattern] = $controllerAction;
    }

    public function dispatch()
    {
        $currentMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $uriPattern = $this->getRelativeUri();


        if (!isset($this->routes[$currentMethod])
            OR !isset($this->routes[$currentMethod][$uriPattern])
        ) {
            throw new \Exception('Route not found');
        }


        $controllerAndAction = $this->routes[$currentMethod][$uriPattern];
        $controllerActionArray = explode('@', $controllerAndAction);

        $controller = $controllerActionArray[0];
        $action = $controllerActionArray[1];
        $controllerObject = new $controller();

        echo $controllerObject->$action();
    }
}