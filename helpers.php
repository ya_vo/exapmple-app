<?php


function config($name)
{
    $config = [
        'app_base_path' => '/autoloading/',
    ];

    if (!isset($config[$name])) {
        throw new Exception('No such config variable');
    }

    return $config[$name];
}