<?php

require 'vendor/autoload.php';
include 'helpers.php';

$routing = new \App\Services\RoutingService();

$routing->map('get','users','App\Controllers\UserController@index');

$routing->dispatch();